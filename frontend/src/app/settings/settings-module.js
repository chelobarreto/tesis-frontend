/**
 * Creates and initilizes the module settings
 */

(function() {
    'use strict';

    
    angular
        .module('frontend.settings', []);
    
})();
