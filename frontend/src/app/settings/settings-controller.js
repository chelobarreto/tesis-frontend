(function() {
    'use strict';

    angular
        .module('frontend.settings')
        .controller('SettingsCtrl', ['$rootScope', '$scope','$http', '$window', SettingsCtrl]);
    
    function SettingsCtrl($rootScope, $scope, $http, $window) {
        if (angular.isUndefined($rootScope.controllerIP)){
            var str = $window.location.hostname;
            $rootScope.controllerIP = str.match(/([a-z0-9.]+)([:]?)/)[1];
            $rootScope.controllerPort = '8080';
        }
        $scope.submit = function(text1, text2) {
            $rootScope.controllerIP = text1;
            $rootScope.controllerPort = text2;
            $window.location.href='/#/topologia';
        };
       
    }

})();
