(function() {
    'use strict';

    angular
        .module('frontend.settings')
        .config( settingsRoute);


    /* @ngInject */
    function settingsRoute($routeProvider) {
        $routeProvider
            .when('/settings', { //Default
                controller: 'SettingsCtrl',
                templateUrl: 'settings/views/settings.html'
            });

    }

})();
