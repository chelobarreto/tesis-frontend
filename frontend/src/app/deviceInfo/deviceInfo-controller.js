(function() {
    'use strict';

    angular
        .module('frontend.deviceInfo')
        .controller('DeviceInfoCtrl',  ['$rootScope','$scope','$http','$routeParams', '$window', DeviceInfoCtrl]);
    //////////////////////

    /**
     * @ngdoc function
     * @name frontend.deviceInfo.controller:DeviceInfoCtrl
     * @description
     * # DeviceInfoCtrl
     * Controller of the frontend.deviceInfo
     * @ngInject
     */
     function DeviceInfoCtrl($rootScope,$scope, $http, $routeParams, $window) {
        $scope.deviceId= $routeParams.deviceId;
        $http(
            {
                method : 'GET',
                url : 'http://' + $rootScope.controllerIP + ':' + 
                $rootScope.controllerPort + '/device/' + $scope.deviceId
            })
        .then(
            function mySucces(response) {
                $scope.data = {};
                $scope.data = response.data;
            }, 
            function myError(response) {
                $scope.controllerError = 'Controller Comnunication Error';
            }
        );
    }

})();
