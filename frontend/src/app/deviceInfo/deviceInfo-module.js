/**
 * Creates and initilizes the module deviceInfo
 */

(function() {
    'use strict';

    
    angular
        .module('frontend.deviceInfo', [], moduleConfiguration);

    /* @ngInject */
    function moduleConfiguration() {
        //TODO Have any module specific configurator here
    }
    
})();
