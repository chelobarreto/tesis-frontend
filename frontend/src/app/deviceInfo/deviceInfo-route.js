(function() {
    'use strict';

    angular
        .module('frontend.deviceInfo')
        .config( deviceInfoRoute);


    /* @ngInject */
    function deviceInfoRoute($routeProvider) {
        $routeProvider
            .when('/deviceInfo/:deviceId', { //Default
                controller: 'DeviceInfoCtrl',
                templateUrl: 'deviceInfo/views/deviceInfo.html'
            });

    }

})();
