(function() {
    'use strict';

    angular
        .module('frontend.topologia')
        .config( topologiaRoute);


    /* @ngInject */
    function topologiaRoute($routeProvider) {
        $routeProvider
            .when('/topologia', { //Default
                controller: 'TopologiaCtrl',
                templateUrl: 'topologia/views/topologia.html'
            });

    }

})();
