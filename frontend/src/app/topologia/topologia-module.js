/**
 * Creates and initilizes the module topologia
 */

(function() {
    'use strict';

    
    angular
        .module('frontend.topologia', [], moduleConfiguration);

    /* @ngInject */
    function moduleConfiguration() {
        //TODO Have any module specific configurator here
    }
    
})();
