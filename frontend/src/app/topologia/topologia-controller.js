(function() {
    'use strict';

    angular
        .module('frontend.topologia')
        .controller('TopologiaCtrl', ['$rootScope','$scope','$http', '$window', TopologiaCtrl]);
    function TopologiaCtrl($rootScope,$scope, $http, $window) {
      $scope.onSelectedElement = function(element) {
        $window.location.href='/#/deviceInfo/' + element.dpid;
      }; 
      $http(
      {
        method : 'GET',
        url : 'http://' + $rootScope.controllerIP + ':' + $rootScope.controllerPort + '/nib'
      })
      .then(
        function mySucces(response) {
          $scope.data = {};
          $scope.data.nodes = response.data.nodes;
          $scope.data.links = response.data.links;
        }, 
        function myError(response) {
          $scope.controllerError = 'Controller Comnunication Error';
        }
      );
    }
})();