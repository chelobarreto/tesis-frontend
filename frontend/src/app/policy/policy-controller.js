(function() {
    'use strict';

    angular
        .module('frontend.policy')
        .controller('PolicyCtrl', ['$rootScope','$scope','$http', '$window', PolicyCtrl]);
    
    function PolicyCtrl($rootScope,$scope, $http, $window) {
      $scope.policy = '';

      /**
      Compilación de la Política
      **/
      $scope.compile = function(text) {
        $http(
              {
                method: 'POST',
                data: text,
                crossDomain: true,
                url : 'http://' + $rootScope.controllerIP + ':' + $rootScope.controllerPort + '/compile_policy',
              }
            )
          .then(
          function mySucces(response) {
	    $window.alert('Compilacion Exitosa');
            $scope.compiledFlows = JSON.stringify(response.data, null, '\t');
            delete $scope.error;
          }, 
          function myError(response) {
	    $window.alert(response.data.message);
            $scope.error = response.data;
          }
        );
      };

      /**
      Instalacion de la Politica
      **/
      $scope.submit = function(text) {
        $http(
              {
                method: 'POST',
                data: text,
                crossDomain: true,
                url : 'http://' + $rootScope.controllerIP + ':' + $rootScope.controllerPort + '/policy',
              }
            )
          .then(
            function mySucces(response) {
	      $window.alert('Instalacion Exitosa');
              $scope.data = response.data;
              delete $scope.error;
            }, 
            function myError(response) {
	      $window.alert(response.data.message);
              $scope.error = response.data;
            }
          );
      };
      /**
      Obtención de la política Instalada
      **/
      $http(
      {
        method : 'GET',
        url : 'http://' + $rootScope.controllerIP + ':' + $rootScope.controllerPort + '/policy',
      })
      .then(
        function mySucces(response) {
          //$scope.data = {};
          $scope.installedPolicy = response.data;
        }, 
        function myError(response) {
          $scope.error = 'Controller Comnunication Error';
        }
      );
    }
})();
