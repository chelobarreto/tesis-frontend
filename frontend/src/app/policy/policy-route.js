(function() {
    'use strict';

    angular
        .module('frontend.policy')
        .config( policyRoute);


    /* @ngInject */
    function policyRoute($routeProvider) {
        $routeProvider
            .when('/policy', { //Default
                controller: 'PolicyCtrl',
                templateUrl: 'policy/views/policy.html'
            });

    }

})();
