/**
 * Creates and initilizes the module policy
 */

(function() {
    'use strict';

    
    angular
        .module('frontend.policy', [], moduleConfiguration);

    /* @ngInject */
    function moduleConfiguration() {
        //TODO Have any module specific configurator here
    }
    
})();
