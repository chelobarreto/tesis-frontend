(function() {
'use strict';

angular
    .module('frontend.d3')
    .directive('d3Graph',['$window','d3Service', d3GraphDirective]);

    /* @ngInject */
    function d3GraphDirective ($window, d3Service) {
        var directive = {
            templateUrl: 'd3/views/d3graph.html', 
            link: link,
            restrict: 'EA',
            scope: {
                data: '=',
                selectedElement: '=',
                onSelectedElement: '&'
            }
        };

        function link(scope, element, attrs) {
            d3Service.d3().then(
                function(d3){
                    /**
                    Ancho de la ventana sobre 12 columnas sistema grid de bootstrap
                    **/
                    var columnWidth = parseInt($window.innerWidth / 12);
                    /** Desplazamiento 2 columnas a izquierda ***/
                    var offset = columnWidth +(2*columnWidth);
                    offset = offset + (6*columnWidth);
                    var fill = d3.scale.category20();
                    var svg = d3.select('#graphId');
                    //var nodeById = d3.map();
                    /*scope.data.nodes.forEach(function(node) {
                        nodeById.set(node.id, node);
                    });
                    scope.data.links.forEach(function(link) {
                        link.source = nodeById.get(link.source);
                        link.target = nodeById.get(link.target);
                    });*/
                    /* Force Layout */
                    var topo = d3.layout.force()
                    .charge(-100)
                    .linkDistance(50)
                    .nodes(scope.data.nodes)
                    .links(scope.data.links)
                    .size([offset,offset/3])
                    .start();

                    /* Links */
                    
                    var link = svg.append('svg:g').selectAll('line.link')
                    .data(scope.data.links)
                    .enter().append('svg:line')
                    .attr('class', 'link')
                    .style('stroke-width', 
                        function(d) { 
                            return Math.sqrt(d.value); 
                        }
                    )
                    .attr('x1', function(d) { return d.source.x; })
                    .attr('y1', function(d) { return d.source.y; })
                    .attr('x2', function(d) { return d.target.x; })
                    .attr('y2', function(d) { return d.target.y; });
		   
                    /* Nodos */
                    var node = svg.append('svg:g').selectAll('circle.node')
                    .data(scope.data.nodes)
                    .enter().append('svg:circle')
                    .attr('class', 'node')
                    .attr('r', 15)
                    .style('fill', 
                        function(d) { 
                            return fill(d.group); 
                        }
                    )
                    .attr('cx',
                        function(d) { 
                            return d.x; 
                        }
                    )
                    .attr('cy', 
                        function(d) { 
                            return d.y; 
                        }
                    )
                    .call(topo.drag);

                    /* Pinning Nodes */
                    var nodeDrag = d3.behavior.drag()
                    .on('dragstart', dragstart)
                    .on('drag', dragmove)
                    .on('dragend', dragend);
    
                    function dragstart(d, i) {
                        // stops the force auto positioning before you start dragging
                        topo.stop();
                    }
                    
                    function dragmove(d, i) {
                        d.px += d3.event.dx;
                        d.py += d3.event.dy;
                        d.x += d3.event.dx;
                        d.y += d3.event.dy;
                    }
    
                    function dragend(d, i) {
                        /* of course set the node to fixed so the force doesn't include 
                        the node in its auto positioning stuff */
                        d.fixed = true; 
                        topo.resume();
                    }

                    function releasenode(d) {
                        /* of course set the node to fixed so the force doesn't include 
                        the node in its auto positioning stuff*/
                        d.fixed = false; 
                        topo.resume();
                    }
                    
                    node.on('dblclick', releasenode);
                    node.call(nodeDrag);
                    
                    /* Menu Contextual */
                    
                    /*node.on('click', 
                        function(d, i) {
                            scope.onSelectedElement({'element': d});
                        });*/
                    
                    /* Text */
                    var text = svg.append('svg:g').selectAll('g')
                    .data(topo.nodes())
                    .enter().append('svg:g');
                    
                    text.append('svg:text')
                    .text(function(d) { return d.id;});

                    svg.style('opacity', 1e-6)
                    .transition()
                    .duration(1000)
                    .style('opacity', 1);

                    topo.on('tick', function() {
                        link.attr('x1', 
                            function(d) { 
                                return d.source.x;
                            }
                        )
                        .attr('y1', function(d) { return d.source.y; })
                        .attr('x2', function(d) { return d.target.x; })
                        .attr('y2', function(d) { return d.target.y; });
                        text.attr('transform', 
                            function(d) { 
                                return 'translate(' + d.x + ',' + d.y + ')'; 
                            }
                        );

                        node.attr('cx', function(d) { return d.x; })
                        .attr('cy', function(d) { return d.y; });
                    });
                }
            );
        }
        return directive;
    }
})();
