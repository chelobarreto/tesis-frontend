/**
 * Creates and initilizes the module d3
 */

(function() {
    'use strict';

    
    angular
        .module('frontend.d3', [], moduleConfiguration);

    /* @ngInject */
    function moduleConfiguration() {
        //TODO Have any module specific configurator here
    }
    
})();
