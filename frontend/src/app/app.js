(function() {
    'use strict';
    //This is the basic entry point of the applicaion
    angular.module('frontend', [
        'ngRoute', 'ngResource','frontend.topologia', 'frontend.d3', 'frontend.deviceInfo', 
        'frontend.policy', 'frontend.settings'
    ]);

})();
